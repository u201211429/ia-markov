/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package chains;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.UUID;

/**
 *
 * @author Jean Pierre
 */
public class Markov {
    Random r =  new Random();
    int levels = 2;
    HashMap<String, List<String>> chain = new HashMap<>();
    
    public void MarkovMaker(List<List<String>> database) {
        for (List<String> item : database) {
            processSong(item);
        }
    }
    public void processSong(List<String> song) {
        //key strings for start and finish
        chain.clear();
        for(int i = 0; i < levels; i++)
            song.add(0, "Start".concat(String.valueOf(levels-i)));
        song.add("Finish");
        
        for(int i = 0; i < song.size() - levels; i++) {
            String keyString = join(song.subList(i, i+levels));
            if(chain.containsKey(keyString)) {
                chain.get(keyString).add(song.get(i+levels));
            }
            else {
                List<String> tempVar = new ArrayList<>();
                tempVar.add(song.get(i+levels));
                chain.put(keyString, tempVar );
            }
        }
    }
    
    private String join(List<String> list) {
        String ans = "";
        for(int i = 0; i < list.size(); i++) {
            if(i != 0)
                ans = ans.concat("|");
            ans = ans.concat(list.get(i));
        }
        return ans;
    }
    
    public Map<String, List<String>> getChain() {
        return chain;
    }
    private boolean isPermited(List<String> list) {
        String temp = "";
        for (String item : list) {
            if (item.compareTo("Finish") != 0) {
                temp = item;
            }
        }
        if(temp.compareTo("") == 0)
            return false;   
        for (String item : list) {
            if (item.compareTo("Finish") != 0) {
                if (item.compareTo(temp) != 0) {
                    return true;
                }
            }
        }
        
        return false;
    }
    public List<String> getNewResult(int maxSize, boolean untilMaxSize) {
        //Cambia de seed cada vez que corre el algoritmo
        r.setSeed(UUID.randomUUID().getMostSignificantBits());
        List<String> ans = new ArrayList<>();
        for(int i = 0; i < levels; i++)
            ans.add(0, "Start".concat(String.valueOf(levels-i)));
        for(int i = 0; i < maxSize; i++) {
            String key = join(ans.subList(i, i + levels));            
            List<String> list = chain.get(key);
            String toAdd = list.get(r.nextInt(list.size()));
            if(untilMaxSize) {//Si es que tiene que llegar a la cantidad de caracteres
                if(toAdd.compareTo("Finish") == 0) {//Si es el final
                    i--;//Retrocede y no lo agrega
                    while(!isPermited(list)) { 
                       ans.remove(ans.size() - 1);
                       key = join(ans.subList(i, i+levels));
                       list = chain.get(key);
                       i--;
                    }
                }
                else
                    ans.add(toAdd);
            }
            else {
                if(toAdd.compareTo("Finish") == 0)//Si es el final, corta el bucle
                    break;
                ans.add(toAdd);
            }
        }
        ans.add("Finish");
        
        return ans.subList(levels, ans.size()-1);     
    }
    public void setLevel(int level) {
        if (level >= 1 && level <= 4 ) {
            this.levels = level;
        }
    }
}

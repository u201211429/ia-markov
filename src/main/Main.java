package main;

import java.io.File;
import java.util.List;
import chains.Markov;
import gui.guiFrame;
import javax.swing.UIManager;
import org.jfugue.player.Player;
import parsers.JFugueGenerator;
import parsers.BachChoraleParser;
/**
 *
 * @author Jamil
 */
public class Main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {        
        try {
            javax.swing.UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(guiFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(guiFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(guiFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(guiFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new guiFrame().setVisible(true);
            }
        });
    }
}


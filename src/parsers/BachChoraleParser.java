/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package parsers;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 *
 * @author Jamil
 */
public class BachChoraleParser {
    List<List<String>> pitch = new ArrayList<>();   
    List<List<String>> duration = new ArrayList<>();

    public BachChoraleParser() {
    }
    
    public void read() {
        
        BufferedReader br = null;        
        try {
            br = new BufferedReader(new InputStreamReader(getClass().getResourceAsStream("/resources/chorales.lisp")));
            String line = br.readLine();
            while (line != null) {
                pitch.add(processPitch(line));
                duration.add(processDuration(line));
                line = br.readLine();
            }
            br.close();
        } catch (IOException e) {
            System.out.println(e.getMessage());
        }
    }

    private List<String> processPitch(String s) {
        List<String> pitches = new ArrayList<String>();
        Matcher m = Pattern.compile("pitch ([0-9]{1,})").matcher(s);
        while (m.find()) {
            pitches.add(m.group(1));
        }
        return pitches;
    }

    private List<String> processDuration(String s) {
        List<String> pitches = new ArrayList<String>();
        Matcher m = Pattern.compile("dur ([0-9]{1,})").matcher(s);
        while (m.find()) {
            pitches.add(m.group(1));
        }
        return pitches;
    }

    public List<List<String>> getDuration() {
        return duration;
    }

    public List<List<String>> getPitch() {
        return pitch;
    }
}

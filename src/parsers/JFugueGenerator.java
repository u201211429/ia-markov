/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package parsers;

import java.util.List;

/**
 *
 * @author jamil
 */
public class JFugueGenerator {

    public JFugueGenerator() {
    }
    
    public String parse(List<String> pitches, List<String> durations, int speed) {
        StringBuilder builder = new StringBuilder();
        for (int i = 0; i<pitches.size(); i++) {
            Double duration = Double.parseDouble(durations.get(i))/speed;
            
            String temp = pitches.get(i)+"/"+duration.toString();
            builder.append(temp);
            builder.append(" ");      
            
        }
        return builder.toString();
    }  
}
